import json
import pandas as pd
from datetime import timedelta

pull_requests = 'geth-pull-requests.jsonl'
#pull_requests = '/kaggle/input/gethpullrequestsjsonl/geth-pull-requests.jsonl'
with open(pull_requests) as f:
    lines = [json.loads(l) for l in f.readlines()]
df = pd.json_normalize(lines, sep='_')
df['closedAt'] = pd.to_datetime(df.closedAt)
df['createdAt'] = pd.to_datetime(df.createdAt)
df['mergedAt'] = pd.to_datetime(df.mergedAt)
df['days'] = (df.closedAt - df.createdAt).dt.days
df['createdWeekStart'] = df.createdAt.apply(lambda x: (x - timedelta(days=x.dayofweek)).date())

closed = df[df.closedAt.notnull()]
closed['closingWeekStart'] = closed.closedAt.apply(lambda x: (x - timedelta(days=x.dayofweek)).date())
groups = closed.groupby(by=['closingWeekStart']).agg({'days': ['median', 'mean', 'max'], 'closingWeekStart': 'count'})

team = set(['karalabe', 'fjl', 'holiman', 'rjl493456442', 'obscuren', 'zsfelfoldi', 'gballet', 'bas-vk', 'nonsense',
            'gluk256', 'janos', 'acud', 'Arachnid', 'holisticode', 'zelig', 'nolash', 'kurkomisi', 'Gustav-Simonsson',
            'MariusVanDerWijden', 'frncmx', 'adamschmideg'])
external = df[~ df.author_login.isin(team)]
external_prs = external.groupby(external.createdWeekStart).agg({'days': 'count'})
total_prs = df.groupby(df.createdWeekStart).agg({'days': 'count'})
ratio = external_prs / total_prs

#%matplotlib inline
